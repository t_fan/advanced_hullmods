package data.hullmods;

import java.util.HashMap;
import java.util.Map;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

import com.fs.starfarer.api.combat.ShipAPI; // Needed for vanilla incompatibility catch - I think.
import data.scripts.util.MagicIncompatibleHullmods; //Needed for handling incompatibilities with hullmods not in my mod.
import com.fs.starfarer.api.Global; // Needed to grab the alert sound to play.

public class sd_AHM_UnstableInjectorAdvanced extends BaseHullMod {

	private static Map mag = new HashMap();
	static {
		mag.put(HullSize.FRIGATE, 19f);
		mag.put(HullSize.DESTROYER, 15f);
		mag.put(HullSize.CRUISER, 11f);
		mag.put(HullSize.CAPITAL_SHIP, 11f);
	}
	
	private static final String NOTIFICATION_SOUND = "cr_allied_critical"; //This is the alert sound to play; seems to be the community standard for hullmod incompatibilities.
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getMaxSpeed().modifyFlat(id, (Float) mag.get(hullSize));
		if(stats.getVariant().getHullMods().contains("unstable_injector")){
			MagicIncompatibleHullmods.removeHullmodWithWarning(stats.getVariant(), "sd_AHM_UnstableInjectorAdvanced", "unstable_injector"); //Looks for the hullmod above under getHullMods().contains("foo"); if found, removes the first hullmod listed in this line, and blames the second.
			Global.getSoundPlayer().playUISound(NOTIFICATION_SOUND, 1f, 1f); //Plays the alert sound.
		}
		if(stats.getVariant().getHullMods().contains("safetyoverrides")){
			MagicIncompatibleHullmods.removeHullmodWithWarning(stats.getVariant(), "sd_AHM_UnstableInjectorAdvanced", "safetyoverrides"); //Looks for the hullmod above under getHullMods().contains("foo"); if found, removes the first hullmod listed in this line, and blames the second.
			Global.getSoundPlayer().playUISound(NOTIFICATION_SOUND, 1f, 1f); //Plays the alert sound.
		}
	}
	
	public String getDescriptionParam(int index, HullSize hullSize) {
		if (index == 0) return "" + ((Float) mag.get(HullSize.FRIGATE)).intValue();
		if (index == 1) return "" + ((Float) mag.get(HullSize.DESTROYER)).intValue();
		if (index == 2) return "" + ((Float) mag.get(HullSize.CRUISER)).intValue();
		if (index == 3) return "" + ((Float) mag.get(HullSize.CAPITAL_SHIP)).intValue();

		return null;
	}
	
	@Override
	public boolean isApplicableToShip(ShipAPI ship) {
		return !ship.getVariant().getHullMods().contains("unstable_injector") && !ship.getVariant().getHullMods().contains("safetyoverrides");
	}
	
	public String getUnapplicableReason(ShipAPI ship) {
		if (ship.getVariant().getHullMods().contains("unstable_injector")) {
			return "Advanced Unstable Injectors are mutually incompatible with the conventional implementation.";
		}
		if (ship.getVariant().getHullMods().contains("safetyoverrides")) {
			return "Advanced Unstable Injectors cannot have their safeties overridden.";
		}
		return null;
	}

}