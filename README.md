# Advanced Hullmods

A Modification for the game Starsector, originally created by ShadowDragon8685 and published on the starsector forums: https://fractalsoftworks.com/forum/index.php?topic=18504.0  
I have his permission to publish updates to this mod.  
Feel free to create any Merge Requests, I will review them periodically.