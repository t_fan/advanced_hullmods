package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

import com.fs.starfarer.api.combat.ShipAPI; // Needed for vanilla incompatibility catch - I think.
import data.scripts.util.MagicIncompatibleHullmods; //Needed for handling incompatibilities with hullmods not in my mod.
import com.fs.starfarer.api.Global; // Needed to grab the alert sound to play.

public class sd_AHM_ArmoredWeaponsAdvanced extends BaseHullMod {

	public static final float HEALTH_BONUS = 75f;
	public static final float ARMOR_BONUS = 7.5f;
	private static final String NOTIFICATION_SOUND = "cr_allied_critical"; //This is the alert sound to play; seems to be the community standard for hullmod incompatibilities.
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getArmorBonus().modifyPercent(id, ARMOR_BONUS);
		stats.getWeaponHealthBonus().modifyPercent(id, HEALTH_BONUS);
		if(stats.getVariant().getHullMods().contains("armoredweapons")){
			MagicIncompatibleHullmods.removeHullmodWithWarning(stats.getVariant(), "sd_AHM_ArmoredWeaponsAdvanced", "armoredweapons"); //Looks for the hullmod above under getHullMods().contains("foo"); if found, removes the first hullmod listed in this line, and blames the second.
			Global.getSoundPlayer().playUISound(NOTIFICATION_SOUND, 1f, 1f); //Plays the alert sound.
		}
	}
	
	public String getDescriptionParam(int index, HullSize hullSize) {
		if (index == 0) return "" + (int) HEALTH_BONUS + "%";
		if (index == 1) return "" + (float) ARMOR_BONUS + "%";
		return null;
	}

	@Override
	public boolean isApplicableToShip(ShipAPI ship) {
		return !ship.getVariant().getHullMods().contains("armoredweapons");
	}
	
	public String getUnapplicableReason(ShipAPI ship) {
		if (ship.getVariant().getHullMods().contains("armoredweapons")) {
			return "Advanced and conventional Armor for weapon mounts are mutually exclusive. You cannot install Advanced Armored Weapon Mounts when the conventional version are present.";
		}
		return null;
	}

}