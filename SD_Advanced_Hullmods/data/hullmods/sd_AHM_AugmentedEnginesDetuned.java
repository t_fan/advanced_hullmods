package data.hullmods;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.hullmods.BaseLogisticsHullMod;

import com.fs.starfarer.api.combat.ShipAPI; // Needed for vanilla incompatibility catch - I think.
import data.scripts.util.MagicIncompatibleHullmods; //Needed for handling incompatibilities with hullmods not in my mod.
import com.fs.starfarer.api.Global; // Needed to grab the alert sound to play.
import com.fs.starfarer.api.impl.campaign.ids.HullMods; // Need to reimplement vanilla logistics-inapplicability checking.
import com.fs.starfarer.api.impl.campaign.ids.Stats; // Need to reimplement vanilla logistics-inapplicability checking.
import com.fs.starfarer.api.loading.HullModSpecAPI; // Need to reimplement vanilla logistics-inapplicability checking.


public class sd_AHM_AugmentedEnginesDetuned extends BaseLogisticsHullMod {
	
	private static final int BURN_LEVEL_BONUS = -1; //I honestly thought this would have to be an integer.
	private static final float FUEL_MULT = 0.7f; //With Efficiency Overhaul can reduce fuel usage to 50%
	private static final String NOTIFICATION_SOUND = "cr_allied_critical"; //This is the alert sound to play; seems to be the community standard for hullmod incompatibilities.
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getMaxBurnLevel().modifyFlat(id, BURN_LEVEL_BONUS);
		stats.getFuelUseMod().modifyMult(id, FUEL_MULT);
		if(stats.getVariant().getHullMods().contains("augmentedengines")){
			MagicIncompatibleHullmods.removeHullmodWithWarning(stats.getVariant(), "sd_AHM_AugmentedEnginesDetuned", "augmentedengines"); //Looks for the hullmod above under getHullMods().contains("foo"); if found, removes the first hullmod listed in this line, and blames the second.
			Global.getSoundPlayer().playUISound(NOTIFICATION_SOUND, 1f, 1f); //Plays the alert sound.
		}
	}
	
	public String getDescriptionParam(int index, HullSize hullSize, ShipAPI ship) {
		if (index == 1) return "" + BURN_LEVEL_BONUS;
		if (index == 0) return "" + (int) Math.round((1f - FUEL_MULT) * 100f) + "%";
		return null;
	}

	@Override
	public boolean isApplicableToShip(ShipAPI ship) {
		boolean has = spec != null && ship.getVariant().hasHullMod(spec.getId());
		int num = getNumLogisticsMods(ship);
		if (has) num--;
		int max = getMax(ship);
		if (num >= max) {
			return false;
		}; // Vanilla "too many Logistics hullmods" checking above this.
		return !ship.getVariant().getHullMods().contains("augmentedengines") && !ship.getVariant().getHullMods().contains("sd_AHM_AugmentedEnginesAdvanced");
	}
	
	public String getUnapplicableReason(ShipAPI ship) {
		if (ship.getVariant().getHullMods().contains("augmentedengines")) {
			return "You cannot tune the same set of engines for both performance and efficiency!";
		}
		if (ship.getVariant().getHullMods().contains("sd_AHM_AugmentedEnginesAdvanced")) {
			return "You cannot tune the same set of engines for both performance and efficiency!";
		}
		return null;
	}

}