//package com.fs.starfarer.api.impl.hullmods;
package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.HullMods;
import com.fs.starfarer.api.impl.campaign.ids.Stats;

public class MilitarizedSubsystems extends BaseHullMod {

	private static int BURN_LEVEL_BONUS = 1;
	private static float MAINTENANCE_PERCENT = 100;
	private static float FLUX_DISSIPATION_PERCENT = 10;
	private static float ARMOR_BONUS = 10;
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getSensorStrength().unmodify(HullMods.CIVGRADE);
		stats.getSensorProfile().unmodify(HullMods.CIVGRADE);
		
		stats.getMaxBurnLevel().modifyFlat(id, BURN_LEVEL_BONUS);

		float mult = getEffectMult(stats);
		stats.getFluxDissipation().modifyPercent(id, FLUX_DISSIPATION_PERCENT * mult);
		stats.getEffectiveArmorBonus().modifyFlat(id, ARMOR_BONUS * mult);
        
		stats.getMinCrewMod().modifyPercent(id, MAINTENANCE_PERCENT);
		
	}
	
    public static float getEffectMult(MutableShipStatsAPI stats) {
        float bonus = getBonusPercent(stats);
        return 1f + bonus / 100f;
	}
    
    public static float getBonusPercent(MutableShipStatsAPI stats) {
		if (Global.getSettings().getCurrentState() == GameState.TITLE) return 0f;
		MutableCharacterStatsAPI cStats = null;
		if (stats == null) {
			cStats = Global.getSector().getPlayerStats();
		} else {
			FleetMemberAPI member = stats.getFleetMember();
			if (member == null) return 0f;
			PersonAPI commander = member.getFleetCommanderForStats();
			if (commander == null) {
				commander = member.getFleetCommander();
			}
			if (commander == null) return 0f;
			cStats = commander.getStats();
		}
		float bonus = cStats.getDynamic().getMod(Stats.AUXILIARY_EFFECT_ADD_PERCENT).computeEffective(0f);
		return Math.round(bonus);
	}
    
	public String getDescriptionParam(int index, HullSize hullSize) {
		if (index == 0) return "" + BURN_LEVEL_BONUS;
		float mult = getEffectMult(null);
		if (index == 1) return "" + (int) Math.round(FLUX_DISSIPATION_PERCENT * mult) + "%";
		if (index == 2) return "" + (int) Math.round(ARMOR_BONUS * mult);
		if (index == 3) return "" + (int)Math.round(MAINTENANCE_PERCENT) + "%";
		return null;
	}
    
	@Override
	public boolean isApplicableToShip(ShipAPI ship) {
		return ship.getVariant().hasHullMod(HullMods.CIVGRADE) && super.isApplicableToShip(ship);
	}

	@Override
	public String getUnapplicableReason(ShipAPI ship) {
		if (!ship.getVariant().hasHullMod(HullMods.CIVGRADE)) {
			return "Can only be installed on civilian-grade hulls";
		}
		return super.getUnapplicableReason(ship);
	}
	
	
	
	
}

