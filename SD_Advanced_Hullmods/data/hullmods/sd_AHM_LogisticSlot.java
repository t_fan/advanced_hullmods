package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.hullmods.BaseLogisticsHullMod; // Let me see if this fixes it...
import com.fs.starfarer.api.impl.campaign.ids.Stats; //Needed this to grab MAX_LOGISTICS_HULLMODS_MOD

import com.fs.starfarer.api.combat.ShipAPI; // Needed for vanilla incompatibility catch - I think.
// import data.scripts.util.MagicIncompatibleHullmods; //Needed for handling incompatibilities with hullmods not in my mod.
import com.fs.starfarer.api.Global; // Needed to grab the alert sound to play.

public class sd_AHM_LogisticSlot extends BaseHullMod {
	
	private static final int LOGISTICS_HULLMOD_SLOTS_BONUS = 1;
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getDynamic().getMod(Stats.MAX_LOGISTICS_HULLMODS_MOD).modifyFlat(id, LOGISTICS_HULLMOD_SLOTS_BONUS);
	}
	
	public String getDescriptionParam(int index, HullSize hullsize) {
		if (index == 0) return "" + LOGISTICS_HULLMOD_SLOTS_BONUS;
		if (index == 1) return "Caution: The shipyard UI tooltip which informs you of the maximum number of logistics hullmods when explaining why additional logistics hullmods may not be installed will not account for the presence of this hullmod.";
		return null;
	}
	
}





