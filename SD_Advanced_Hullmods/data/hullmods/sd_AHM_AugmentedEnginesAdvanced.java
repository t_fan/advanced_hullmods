package data.hullmods;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.hullmods.BaseLogisticsHullMod;

import com.fs.starfarer.api.combat.ShipAPI; // Needed for vanilla incompatibility catch - I think.
import data.scripts.util.MagicIncompatibleHullmods; //Needed for handling incompatibilities with hullmods not in my mod.
import com.fs.starfarer.api.Global; // Needed to grab the alert sound to play.
import com.fs.starfarer.api.impl.campaign.ids.HullMods; // Need to reimplement vanilla logistics-inapplicability checking.
import com.fs.starfarer.api.impl.campaign.ids.Stats; // Need to reimplement vanilla logistics-inapplicability checking.
import com.fs.starfarer.api.loading.HullModSpecAPI; // Need to reimplement vanilla logistics-inapplicability checking.

public class sd_AHM_AugmentedEnginesAdvanced extends BaseLogisticsHullMod {
	
	private static final int BURN_LEVEL_BONUS = 1;
	private static final String NOTIFICATION_SOUND = "cr_allied_critical"; //This is the alert sound to play; seems to be the community standard for hullmod incompatibilities.
	
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
		stats.getMaxBurnLevel().modifyFlat(id, BURN_LEVEL_BONUS);
		if(stats.getVariant().getHullMods().contains("augmentedengines")){
			MagicIncompatibleHullmods.removeHullmodWithWarning(stats.getVariant(), "sd_AHM_AugmentedEnginesAdvanced", "augmentedengines"); //Looks for the hullmod above under getHullMods().contains("foo"); if found, removes the first hullmod listed in this line, and blames the second.
			Global.getSoundPlayer().playUISound(NOTIFICATION_SOUND, 1f, 1f); //Plays the alert sound.
		}
	}
	
	public String getDescriptionParam(int index, HullSize hullSize) {
		if (index == 0) return "" + BURN_LEVEL_BONUS;
		return null;
	}

	@Override
	public boolean isApplicableToShip(ShipAPI ship) {
		boolean has = spec != null && ship.getVariant().hasHullMod(spec.getId());
		int num = getNumLogisticsMods(ship);
		if (has) num--;
		int max = getMax(ship);
		if (num >= max) {
			return false;
		}; // Vanilla "too many Logistics hullmods" checking above this.
		return !ship.getVariant().getHullMods().contains("augmentedengines") && !ship.getVariant().getHullMods().contains("sd_AHM_AugmentedEnginesDetuned");
	}
	
	public String getUnapplicableReason(ShipAPI ship) {
		if (ship.getVariant().getHullMods().contains("augmentedengines")) {
			return "Drive Field Tweaks are a less-intensive, partial implementation of the Augmented Drive Field. You cannot implement Augmented Drive Field without already implementing Drive Field Tweaks, and so the hullmods are mutually exclusive.";
		}
		if (ship.getVariant().getHullMods().contains("sd_AHM_AugmentedEnginesDetuned")) {
			return "You cannot tune the same set of engines for both performance and efficiency!";
		}
		boolean has = spec != null && ship.getVariant().hasHullMod(spec.getId());
		int num = getNumLogisticsMods(ship);
		if (has) num--;
		int max = getMax(ship);
		if (num >= max) {
			String text = "many";
			if (max == 1) text = "one";
			else if (max == 2) text = "two";
			else if (max == 3) text = "three";
			else if (max == 4) text = "four";
			text = "" + MAX_MODS;
			return "Maximum of " + text + " non-built-in \"Logistics\" hullmods per hull";
		} // Vanilla "too many hullmods" checking.
		return null;
	}
	protected int getNumLogisticsMods(ShipAPI ship) {
		int num = 0;
		for (String id : ship.getVariant().getHullMods()) {
			if (ship.getHullSpec().isBuiltInMod(id)) continue;
			
			HullModSpecAPI mod = Global.getSettings().getHullModSpec(id);
			if (mod.hasUITag(HullMods.TAG_UI_LOGISTICS)) {
				num++;
			}
		}
		return num;
	} // This whole thing is part of vanilla "Too Many Logistic Hullmods" checking.
}


